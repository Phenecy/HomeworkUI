package dev.phenecy.homeworkui

data class Course(
    val courseId: Int,
    val courseTitle: String,
    val courseCounter: String,
    val courseBackground: String
) {

    class CoursesController {
        val coursesList: MutableList<Course> = mutableListOf()

        init {
            coursesList.add(Course(0, "Вводный курс", "0 из 5", ""))
            coursesList.add(Course(1, "Курс\nночной жор", "2 из 10", ""))
            coursesList.add(Course(2, "Курс\nстань программистом", "4 из 15", ""))
        }
    }
}