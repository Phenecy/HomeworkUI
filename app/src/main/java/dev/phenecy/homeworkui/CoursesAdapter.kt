package dev.phenecy.homeworkui

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class CoursesAdapter(private val coursesList: MutableList<Course>) :
    RecyclerView.Adapter<CoursesAdapter.CoursesHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoursesHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_courses, parent, false)
        return CoursesHolder(view)
    }

    override fun getItemCount(): Int = coursesList.size

    override fun onBindViewHolder(holder: CoursesHolder, position: Int) {
        holder.bind(position)
    }

    inner class CoursesHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(position: Int) {
            val courseTitle = itemView.findViewById<TextView>(R.id.rv_title)
            val courseCounter = itemView.findViewById<TextView>(R.id.rv_text_counter)
            val courseBackground = itemView.findViewById<ImageView>(R.id.card_bg_iv)
            when (position) {
                0 -> {

                    courseCounter.text = coursesList[0].courseCounter
                }
                1 -> {
                    courseTitle.text = coursesList[1].courseTitle
                    courseTitle.setTextColor(Color.WHITE)
                    courseCounter.text = coursesList[1].courseCounter
                    courseBackground.setBackgroundResource(R.drawable.bg_1)
//                    Glide.with(itemView)
//                        .load("https://s3-alpha-sig.figma.com/img/67e8/9a10/a857050b6dffac910e75d002f2eb4434?Expires=1581897600&Signature=f6AWpT3Hn-KSevsP9gDdvTXVCLUjl8jD2ckphlk~N6ugLwJoQjXZWZCXGRLUqD305Q3w7pG6DUBkpXhMXwb-jCeEaECXwvEVGQ23LNBpsA1QFNOAHkbDpVZMjiMBB1LNlOFVRt~qTRwdKBtn~vv2xpYjamYk8xcY3ZQGjYyDWqTQcVQ2BK4sWbnnn~ScO9RetKxZgUv~hTnf~cByEVTmFjfSWGrG5h7HO41EJaMOuswLB4cF6Zfv6WYSA4Nc1DxzLcqSTJCFXTsYB~ZeXH3XDgPEFAkVo55dtulX9SR~EonMgWVKJqbxZ2FNxTCsOEx3aFVlgOPmKW5cbVMkpQMF7Q__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA")
//                        .into(courseBackground)
                }
                2 -> {
                    courseTitle.text = coursesList[2].courseTitle
                    courseCounter.text = coursesList[2].courseCounter
                    courseBackground.setBackgroundResource(R.drawable.bg_2)
//                    Glide.with(itemView)
//                        .load("https://s3-alpha-sig.figma.com/img/f192/7d19/3b9e9aee9489bbfd7db9aa94c4dc953e?Expires=1581897600&Signature=UehG1C1HYEqsbFCtuqXW9lI8hpr27pBGu0Jji06HeXV9ryOBm2GL7IVb9-AQgQ-wJtZVJV11m3OmI98sH8b3lXFrJYJ0ERX4oEAvOo3UdYwdoXVGWrHma9H-B2J9zQzIJOvmY-zZ~Y1JLvn1B3wZThXnTrKxpnHW4Z-g0Z1Iagk1Xq-Thtb9H9z3pwSpF7nHkYnzJu1gHCgKg0GVeiJR5WXTdhVjFSaBEiWeBfFNT3nCowXbrdBln7TBgPjf-27m6x2QAdCMMAbj9ssxyj5W4oMUcLboj9oSZoNj1ecl2hMy~d2T4gJX0J96s-CE3mhwHIv8SMs5LwHEx9~5nRulpQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA")
//                        .into(courseBackground)
                }
            }
        }
    }
}