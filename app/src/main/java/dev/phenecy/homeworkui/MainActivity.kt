package dev.phenecy.homeworkui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*

private lateinit var navController: NavController


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupNavigationDrawer()

    }

    private fun setupNavigationDrawer() {
        navController = Navigation.findNavController(this, R.id.main_fragment)
        bottom_nav_bar.setupWithNavController(navController)
    }

}