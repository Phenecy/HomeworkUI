package dev.phenecy.homeworkui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import dev.phenecy.homeworkui.Course
import dev.phenecy.homeworkui.CoursesAdapter
import dev.phenecy.homeworkui.R
import kotlinx.android.synthetic.main.item_bottom_sheet.*

private lateinit var bottomSheet: ConstraintLayout
private lateinit var viewFragment: View
private lateinit var coursesAdapter: CoursesAdapter
private lateinit var coursesLinearLayoutManager: LinearLayoutManager
private lateinit var coursesRecycler: RecyclerView
private lateinit var animTrans: Animation
private lateinit var animTransReverse: Animation
private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

private var animationListener = object : Animation.AnimationListener {
    override fun onAnimationRepeat(animation: Animation?) {
    }

    override fun onAnimationEnd(animation: Animation?) {

    }

    override fun onAnimationStart(animation: Animation?) {
    }
}

private var coursesList: MutableList<Course> = Course.CoursesController().coursesList

class BioProgramsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewFragment = inflater.inflate(R.layout.fragment_bio_programs, container, false)

        coursesAdapter = CoursesAdapter(coursesList)
        coursesLinearLayoutManager =
            LinearLayoutManager(container!!.context, LinearLayoutManager.HORIZONTAL, false)
        coursesLinearLayoutManager.isSmoothScrollbarEnabled = true
        coursesRecycler = viewFragment.findViewById(R.id.courses_rv)
        coursesRecycler.layoutManager = coursesLinearLayoutManager
        coursesRecycler.adapter = coursesAdapter

        return viewFragment
    }

    private fun initView() {
        animTrans = AnimationUtils.loadAnimation(BioProgramsFragment@ context, R.anim.translate)
        animTrans.setAnimationListener(animationListener)
        animTransReverse =
            AnimationUtils.loadAnimation(BioProgramsFragment@ context, R.anim.translate_reverse)

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

        bottomSheetBehavior.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(p0: View, p1: Float) {

            }

            override fun onStateChanged(p0: View, p1: Int) {

            }
        })
    }

    override fun onResume() {
        super.onResume()

        val coorLayout: CoordinatorLayout = viewFragment.findViewById(R.id.container_bio)
        val viewSlider = layoutInflater.inflate(R.layout.item_bottom_sheet, coorLayout, true)

        viewSlider?.run {
            bottomSheet = findViewById(R.id.bottom_sheet)
        }
        initView()
    }
}